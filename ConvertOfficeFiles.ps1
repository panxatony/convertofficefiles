Set-StrictMode -version 2
cls

$Global:rootpath ="R:\incoming\" #The root of the directory tree where you want to convert your files - should end with
$Global:backuprootpath = "C:\OldFSBackup\" #The root of the directory where a back-up copy of the .doc,ppt and xls files should be placed. Sub-dirs will be created - should end with \


$ErrorActionPreference = 'Inquire'
$WarningPreference = 'Inquire'

$Global:word = new-object -comobject word.application
$Global:PowerP = new-object -comobject PowerPoint.application
$Global:Excel = new-object -comobject Excel.application

$Global:wordSaveFormat = [Enum]::Parse([Microsoft.Office.Interop.Word.WdSaveFormat],”wdFormatDocumentDefault”)
$Global:wordSaveFormatMacro = [Enum]::Parse([Microsoft.Office.Interop.Word.WdSaveFormat],”wdFormatXMLDocumentMacroEnabled”)

$Global:PowerPSaveFormat = [Enum]::Parse([Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType],”ppSaveAsPresentation”)
$Global:ExcelSaveFormat = [Enum]::Parse([Microsoft.Office.Interop.Excel.XlFileFormat],”xlWorkbookDefault”)
$Global:ExcelSaveFormatMacros = [Enum]::Parse([Microsoft.Office.Interop.Excel.XlFileFormat],”xlOpenXMLWorkbookMacroEnabled”)

$Global:outpath = ""
$Global:CurrentFolder =""

function SetFileTimes([string]$oldFilePath,[string] $newFilePath){
   $oldFile = $null
   $newFile = $null
    $oldFile = Get-Item -Path  $oldFilePath
    $newFile = Get-Item -Path $newFilePath
    if($newFile -ne $null){
        if($oldFile -ne $null){
            $NewFile.CreationTime= $oldFile.CreationTime
            $NewFile.LastAccessTime= $oldFile.LastAccessTime
            $NewFile.LastWriteTime = $oldFile.LastWriteTime
        }
    }
}
function BuildPath([string]$ExistingPath, [string]$Extension){
    $TempPath =""
    $TempPath  = ($ExistingPath).substring(0,($ExistingPath).lastindexOf(“.”)).Replace($Global:CurrentFolder,$Global:outpath)
    If($TempPath.length -lt 255){
       $TempPath = $TempPath + $Extension
    }else{
        stop

    }
    return $TempPath
}


function ConvertDocRTF($CurrDocPath){

    Try{
	   $opendoc = $Global:word.documents.open($CurrDocPath,$false,$true) #confirmconversions = false, readonly = true
        $opendoc.ActiveWindow.View = 3 #wdPrintView
       $opendoc.Convert()
    }Catch{
        $opendoc = $null}
    if($opendoc -ne $null){
        if($opendoc.HasVBProject -eq $false){
            $savename = BuildPath $CurrDocPath ".docx"
            Try{
                $opendoc.saveas([ref]“$savename”, [ref]$Global:wordSaveFormat)
            }Catch{$savename = ""}
        }else{
            $savename = BuildPath $CurrDocPath ".docm"
            Try{
                $opendoc.saveas([ref]“$savename”, [ref]$Global:wordSaveFormatMacro)
             }Catch{$savename = ""}
        }

    	$opendoc.close();
        return $savename
        }else{
            return ""
        }
}

function ConvertPPT($CurrDeckPath){

	Try{
        $opendeck = $Global:PowerP.Presentations.open($CurrDeckPath,$false,$true) #confirmconversions = false, readonly = true
    }Catch{
        $opendeck =$null
    }
    if($opendeck -ne $null){
        if($opendeck.HasVBProject -eq $false){
            $savename = BuildPath $CurrDeckPath ".pptx"
        }else{
            $savename = BuildPath $CurrDeckPath ".pptm"
        }
        Try{
            $opendeck.Convert2(“$savename”)
        }Catch{$savename = ""}
    	$opendeck.close();
        return $savename
     }else{
        return ""
     }
}


function ConvertExcel($CurrBookPath){
	$openbook = $null
    Try{
        $openbook = $Global:Excel.Workbooks.open($CurrBookPath,$false,$true) #confirmconversions = false, readonly = true
    }Catch{
        $openbook =$null
    }
    if($openbook -ne $null){
        $openbook.CheckCompatibility =$false

        if($openbook.HasVBProject -eq $false){
            $savename = BuildPath $CurrBookPath ".xlsx"
            Try{
        	   $openbook.SaveAs(“$savename”,$Global:ExcelSaveFormat)
            }Catch{$savename = ""}
        }else{
            $savename = BuildPath $CurrBookPath ".xlsm"
            Try{
        	   $openbook.SaveAs(“$savename”,$Global:ExcelSaveFormatMacros)
            }Catch{$savename = ""}
        }
       	$openbook.close();
        return $savename
    }
}

function LoopFolders($CurrFolderPath){
    $Folders = dir $CurrFolderPath -recurse | where {$_.psiscontainer -eq $true}
    $Folders | ForEach-Object -Process{
        LoopFolders($_.FullName)
    }
    Get-ChildItem -path ($CurrFolderPath + "\*")  -include "*.*" | foreach-object -Process{
        $Global:CurrentFolder=  $CurrFolderPath
        $NewFilePath = ""

        $CurrFilePath = $_.FullName
        $Global:outpath = $CurrFolderPath
        $backuppath = $CurrFolderPath.Replace($Global:rootpath,$Global:backuprootpath)
        If((Test-Path $backuppath ) -eq $false){
            new-item -itemtype Directory -Force -Path $backuppath
        }

        switch($_.Extension.ToLower())
        {
            ".doc"{$NewFilePath =ConvertDocRTF ($CurrFilePath )}
            ".rtf"{$NewFilePath =ConvertDocRTF ($CurrFilePath )}
            ".wpd"{$NewFilePath =ConvertDocRTF ($CurrFilePath )}

            ".ppt"{$NewFilePath =ConvertPPT ($CurrFilePath )}
            ".xls"{$NewFilePath =ConvertExcel ($CurrFilePath )}
            default{}
        }
        if($NewFilePath -ne ""){
            SetFileTimes $CurrFilePath $NewFilePath
            Write-Host $NewFilePath
            #Move-Item $CurrFilePath $backuppath
            Remove-Item $CurrFilePath
        }
    }
}
LoopFolders $Global:rootpath

#Clean up
$Global:word.quit()
$Global:PowerP.quit()
$Global:Excel.quit()
